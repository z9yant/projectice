import { analyzeAndValidateNgModules } from '@angular/compiler';

export class Team {
  id : number;
  abbrev :string;
  name :string;
  logo :string;
}
