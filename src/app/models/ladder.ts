import { analyzeAndValidateNgModules } from '@angular/compiler';

export class Ladder {
  year: number;
  source: string;
  updated: string;
  rank: number;
  mean_rank: string;
  swarms: [string];
  teamid: number;
  team: string;
  sourceid: number;
  round: number;
  percentage: string;
  wins: string;
}
