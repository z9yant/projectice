import { analyzeAndValidateNgModules } from '@angular/compiler';

export class Game{
  id: number;
  date: Date;
  abehinds: number;
  winner: string;
  hbehinds: number;
  ateamid: number;
  is_final: number;
  ascore: number;
  tz: string;
  hscore: number;
  round: number;
  hteamid: number;
  winnerteamid: number;
  year: number;
  hteam: string;
  ateam: string;
  agoals: number;
  hgoals: number;
  venue: string;
  complete: number;
  is_grand_final: number;
  updated: Date;
}
