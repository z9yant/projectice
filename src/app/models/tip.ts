import { analyzeAndValidateNgModules } from '@angular/compiler';

export class Tip {
  ateam: string;
  tip: string;
  gameid: number;
  sourceid: number;
  ateamid: number;
  round: number;
  correct: number;
  hteam: string;
  hteamid: number;
  updated: string;
  year: number;
  confidence: string;
  bits: string;
  source: string;
  err: string;
  hconfidence: string;
  tipteamid: number;
  date: string;
  venue: string;
  margin: string;
}
